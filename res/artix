#!/bin/sh

artixLogo()
{
	CYAN='\033[0;36m'
	NC='\033[0m'
	clear
	echo -e "${CYAN}"
	echo '                   '"'"'                   '
	echo '                  '"'"'o'"'"'                  '
	echo '                 '"'"'ooo'"'"'                 '
	echo '                '"'"'ooxoo'"'"'                '
	echo '               '"'"'ooxxxoo'"'"'               '
	echo '              '"'"'oookkxxoo'"'"'              '
	echo '             '"'"'oiioxkkxxoo'"'"'             '
	echo '            '"'"':;:iiiioxxxoo'"'"'            '
	echo '               `'"'"'.;::ioxxoo'"'"'           '
	echo '          '"'"'-.      `'"'"':;jiooo'"'"'          '
	echo '         '"'"'oooio-..     `'"'"'i:io'"'"'         '
	echo '        '"'"'ooooxxxxoio:,.   `'"'"'-;'"'"'        '
	echo '       '"'"'ooooxxxxxkkxoooIi:-.  `'"'"'       '
	echo '      '"'"'ooooxxxxxkkkkxoiiiiiji'"'"'         '
	echo '     '"'"'ooooxxxxxkxxoiiii:'"'"'`     .i'"'"'     '
	echo '    '"'"'ooooxxxxxoi:::'"'"'`       .;ioxo'"'"'    '
	echo '   '"'"'ooooxooi::'"'"'`         .:iiixkxxo'"'"'   '
	echo '  '"'"'ooooi:'"'"'`                `'"'"''"'"';ioxxo'"'"'  '
	echo ' '"'"'i:'"'"'`                          '"'"''"'"':io'"'"' '
	echo ''"'"'`                                   `'"'"''
	echo '       Artix GNU/Linux Installer    '
	echo '       by: @rafa_99 & @dutchmans    '
	echo '                                    '
	echo -e "${CYAN}"
	echo -e "${NC}"
}

connection()
{
	echo
	echo "################################"
	echo "# Checking Internet Connection #"
	echo "################################"
	echo
	echo
	sleep 2

	if ! ping -q -c 4 artixlinux.org >/dev/null;then
		echo "YOU MUST HAVE INTERNET CONNECTION TO RUN THIS SCRIPT" && exit
	fi
	clear

	echo
	echo "##############################"
	echo "# Quick Fixing Pacman Issues #"
	echo "##############################"
	echo
	echo
	sleep 2

	pacman-key --init
	pacman-key --populate artix
}

vars()
{
	echo
	echo "######################"
	echo "# Set Your Variables #"
	echo "######################"
	echo
	echo
	sleep 2

	# Reading Variables
	## Reading Keyboard Layout
	printf "What is your Keyboard Layout (e.g. us)\n-> " && read -r KEYMAP && loadkeys "$KEYMAP" || exit
	clear

	## Reading Locale
	printf "Select your locale (e.g. en_US)\n-> " && read -r LOCALE
	clear

	## Reading Kernel
	printf "Select your kernel:\n1)linux\n2)linux-hardened\n3)linux-lts [Recommended](Default)\n4)linux-zen\n-> " && read -r ARTIXKERNEL
	case "$ARTIXKERNEL" in
		1)
			ARTIXKERNEL="linux"
			;;
		2)
			ARTIXKERNEL="linux-hardened"
			;;
		4)
			ARTIXKERNEL="linux-zen"
			;;
		*)
			ARTIXKERNEL="linux-lts"
	esac
	clear

	## Reading Init System
	printf "Select your init system:\n1) dinit\n2) OpenRC (default)\n3) Runit\n4) S6\n5) suite66\n-> " && read -r ARTIXINIT

	if ! [ "$ARTIXINIT" -ge 1 ] && ! [ "$ARTIXINIT" -le 5 ]; then
		ARTIXINIT=2;
	fi
	clear

	## Reading Desired Username
	printf "Select your Username (Use only lowercase letters and numbers)\n-> " && read -r USERNAME
	clear

	## Reading Desired Hostname
	printf "Select your Hostname (You can use Uppercase and lowercase letters, numbers and hypens)\n-> " && read -r HOSTNAME
	clear

	## Reading Desired Timezone
	printf "Select the Timezone that better suits you...\nPress ENTER to continue and then press \"Q\" to exit" && read -r TIMEZONE
	find /usr/share/zoneinfo -type d | grep -vi zoneinfo$ | grep -vi etc | xargs -I {} find {} -type f | sed s:"/usr/share/zoneinfo/"::g | less -i
	printf "Which Timezone? (eg America/New_York)\n-> " && read -r TIMEZONE
	clear
}

partitioning()
{
	echo
	echo "#####################"
	echo "# Disk Partitioning #"
	echo "#####################"
	echo
	echo
	sleep 2

	#Partitioning HDD
	## Reading Main Drive
	lsblk
	printf "What is your drive (e.g. sda)\n-> " && read -r SDX
	clear

	## Partitionier
	cfdisk /dev/"$SDX"
	clear

	## Making SWAP Partition
	lsblk
	printf "Do you wish to use a swap parition? [Y/n]" && read -r SWAPUSE
	clear

	if [ "$SWAPUSE" != "N" ] && [ "$SWAPUSE" != "n" ]; then
		{
			lsblk
			echo ""
			echo ""
			printf "What is your swap parition (e.g. sda1)\n-> " && read -r SWAP
			swapoff /dev/"$SWAP" >/dev/null
			clear

			printf "Wipe Swap Partition? [Y/n]" && read -r WIPESWAP
			if [ "$WIPESWAP" != "N" ] && [ "$WIPESWAP" != "n" ]; then
				mkswap /dev/"$SWAP" >/dev/null
			fi
			swapon /dev/"$SWAP" >/dev/null
			clear
		}
	fi
	clear

	## Disk Encryption
	printf "Do you wish to use any encrypted parition? [Y/n]" && read -r ENCRYPTED
	if [ "$ENCRYPTED" != "N" ] && [ "$ENCRYPTED" != "n" ]; then
		{
			pacman --noconfirm --needed -Sy cryptsetup
			clear
			lsblk
			echo ""
			echo ""

			printf "Type the parition you wish to use encrypted (e.g. sda2)\n-> " && read -r ENCRYPTEDPARTITION
			clear

			printf "Do you wish to wipe this encrypted parition? [Y/n]" && read -r WIPENCRYPT
			clear

			if [ "$WIPENCRYPT" != "N" ] && [ "$WIPENCRYPT" != "n" ]; then
				cryptsetup luksFormat /dev/"$ENCRYPTEDPARTITION"
				clear
			fi

			cryptsetup open /dev/"$ENCRYPTEDPARTITION" cryptdevice
			clear
		}
	fi
	clear

	## Mounting Partitions
	echo "Mount and Wipe (if necessary) your partitions"
	echo "After partitioning, type \"exit\" to go back to the script"
	echo ""
	echo ""
	lsblk
	echo ""
	bash
	clear
}

base()
{
	echo
	echo "#####################"
	echo "# Configuring Artix #"
	echo "#####################"
	echo
	echo
	sleep 2

	# Sorting Servers
	echo "Downloading rankmirror script for pacman"
	pacman -Sy --noconfirm --needed pacman-contrib
	clear

	echo "Updating Repos..."
	mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
	grep -vi ^# /etc/pacman.d/mirrorlist.backup | rankmirrors - > /etc/pacman.d/mirrorlist
	clear

	# Bootstrapping Filesystem
	echo "Bootstrapping Filesystem"
	case "$ARTIXINIT" in
		1)
			basestrap /mnt base base-devel dinit elogind-dinit "$ARTIXKERNEL"
			;;
		2)
			basestrap /mnt base base-devel openrc elogind-openrc "$ARTIXKERNEL"
			;;
		3)
			basestrap /mnt base base-devel runit elogind-runit "$ARTIXKERNEL"
			;;
		4)
			basestrap /mnt base base-devel s6 elogind-s6 "$ARTIXKERNEL"
			;;
		5)
			basestrap /mnt base base-devel 66 elogind-suite66 "$ARTIXKERNEL"
	esac
	clear

	fstabgen -U /mnt >> /mnt/etc/fstab
	clear

	# Chroot Filesystem
	## Managing Localtime and Timezone
	rm -rf /mnt/etc/localtime
	artix-chroot /mnt /bin/bash -c "ln -sf /usr/share/zoneinfo/$TIMEZONE /etc/localtime"
	clear

	## Managing Users and Passwords
	artix-chroot /mnt /bin/bash -c "useradd -m -g users -G wheel $USERNAME"
	echo "Enter a password for $USERNAME"
	artix-chroot /mnt /bin/bash -c "passwd $USERNAME"
	clear

	echo "Enter a new password for root account"
	artix-chroot /mnt /bin/bash -c "passwd root"
	clear

	## Adding Systemd Configurations
	case "$ARTIXINIT" in
		2)
			artix-chroot /mnt /bin/bash -c "echo hostname=$HOSTNAME > /etc/conf.d/hostname"
			sed -i s:'keymap="us"':keymap="$KEYMAP":g /mnt/etc/conf.d/keymaps
			;;

		*)
			artix-chroot /mnt /bin/bash -c "echo $HOSTNAME > /etc/hostname"
			artix-chroot /mnt /bin/bash -c "echo KEYMAP=$KEYMAP > /etc/vconsole.conf"
	esac

	artix-chroot /mnt /bin/bash -c "echo LANG=$LOCALE.UTF-8 > /etc/locale.conf"
	artix-chroot /mnt /bin/bash -c "awk 'FNR>23' /etc/locale.gen | grep $LOCALE | sed s:#::g >> /etc/locale.gen"
	artix-chroot /mnt /bin/bash -c "locale-gen"
	clear

	## Adding Encrypted Hook to Init Cpio
	if [ "$ENCRYPTED" != "N" ] && [ "$ENCRYPTED" != "n" ]; then
		{
			artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -Sy cryptsetup"
			HOOKS=$(grep ^HOOKS /mnt/etc/mkinitcpio.conf)
			grep ^HOOKS /mnt/etc/mkinitcpio.conf | sed s:'block':'block encrypt':g \
				| xargs -I {} sed -i s:"$HOOKS":{}:g /mnt/etc/mkinitcpio.conf
			}
	fi
	clear

	## Generating Initramfs
	artix-chroot /mnt /bin/bash -c "mkinitcpio -p $ARTIXKERNEL"
	clear
}

packages()
{
	echo
	echo "#################################"
	echo "# Installation of Base Packages #"
	echo "#################################"
	echo
	echo
	sleep 2

	# Installation of Base Packages
	echo "Installing Base Packages..."
	artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -Syu bash bzip2 cryptsetup device-mapper dhcpcd git sudo grub findutils gawk gcc-libs gettext glibc inetutils iproute2 iputils logrotate lvm2 pacman pciutils procps-ng psmisc shadow sysfsutils texinfo usbutils util-linux jfsutils mdadm reiserfsprogs e2fsprogs xfsprogs"
	clear

	# Enabling Sudo For :wheel Group
	artix-chroot /mnt /bin/bash -c "sed -i s/'# %wheel ALL=(ALL:ALL) ALL'/'%wheel ALL=(ALL:ALL) ALL'/g /etc/sudoers"

	# Installation of Extra Base
	printf "Do you want to install Extra Base Packages? [Y/n]" && read -r EXTRA
	if [ "$EXTRA" != "N" ] && [ "$EXTRA" != "n" ]; then

		artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S base-devel curl os-prober p7zip xdg-user-dirs perl s-nail ntfs-3g"
		clear

		# Xorg Setup
		printf "Do you wish to use a Graphical Interface (Xorg)? [Y/n]" && read -r XORG
		if [ "$XORG" != "N" ] && [ "$XORG" != "n" ]; then
			artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S xorg xorg-apps xorg-server xorg-xinit"
			clear
			artix-chroot /mnt /bin/bash -c "pacman --needed -S xorg-drivers"
		fi
		clear

		# Text Editor Setup
		printf "Select a Text Editor:\n1) emacs\n2) nano\n3) neovim\n4) vi\n5) vim\n-> " && read -r TEXT
		case "$TEXT" in
			1)
				artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S emacs"
				;;
			2)
				artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S nano"
				;;
			3)
				artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S neovim"
				;;
			4)
				artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S vi"
				;;
			5)
				artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S vim"
		esac
		clear

		# WiFi and Ethernet Setup
		printf "Do you wish to use WiFi? [Y/n]" && read -r WIFI
		clear
		if [ "$WIFI" != "N" ] && [ "$WIFI" != "n" ]; then
			printf "Select a Network Manager:\n1) connman\n2) network manager\n-> " && read -r NM
			case "$NM" in
				1)
					artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S connman wpa_supplicant"
					case "$ARTIXINIT" in
						1)
							artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S connman-dinit && ln -s /etc/dinit.d/connmand /etc/dinit.d/boot.d/"
							;;
						2)
							artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S connman-openrc && rc-update add connmand default"
							;;
						3)
							artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S connman-runit && ln -s /etc/runit/sv/connmand /etc/runit/runsvdir/default"
							;;
						4)
							artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S connman-s6 && s6-rc-bundle-update -c /etc/s6/rc/compiled add default connmand"
							;;
						5)
							artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S connman-suite66 && 66-enable -t default connmand"
					esac
					;;
				2)
					artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S networkmanager"
					case "$ARTIXINIT" in
						1)
							artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S networkmanager-dinit && ln -s /etc/dinit.d/NetworkManager /etc/dinit.d/boot.d/"
							;;
						2)
							artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S networkmanager-openrc && rc-update add NetworkManager default"
							;;
						3)
							artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S networkmanager-runit && ln -s /etc/runit/sv/NetworkManager /etc/runit/runsvdir/default"
							;;
						4)
							artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S networkmanager-s6 && s6-rc-bundle-update -c /etc/s6/rc/compiled add default NetworkManager"
							;;
						5)
							artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S networkmanager-suite66 && 66-enable -t default NetworkManager"
					esac
			esac
		else
			artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S dhcpcd"
			case "$ARTIXINIT" in
				1)
					artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S dhcpcd-dinit && ln -s /etc/dinit.d/dhcpcd /etc/dinit.d/boot.d/"
					;;
				2)
					artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S dhcpcd-openrc && rc-update add dhcpcd default"
					;;
				3)
					artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S dhcpcd-runit && ln -s /etc/runit/sv/dhcpcd /etc/runit/runsvdir/default"
					;;
				4)
					artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S dhcpcd-s6 && s6-rc-bundle-update -c /etc/s6/rc/compiled add default dhcpcd"
					;;
				5)
					artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S dhcpcd-suite66 && 66-enable -t default dhcpcd"
			esac
		fi
		clear

		# Installation of Base Fonts
		printf "Do you want to install Base Fonts Packages? [Y/n]" && read -r FONTS
		if [ "$FONTS" != "N" ] && [ "$FONTS" != "n" ]; then
			artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S ttf-bitstream-vera ttf-dejavu gnu-free-fonts adobe-source-code-pro-fonts noto-fonts-cjk noto-fonts-emoji"
		fi
		clear

		# Installation of Proprietary Blobs
		printf "Do you want to install support for Proprietary Firmware? [Y/n]" && read -r BLOBS
		if [ "$BLOBS" != "N" ] && [ "$BLOBS" != "n" ]; then
			artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S linux-firmware"
		fi
		clear

		# Installation of Audio System
		printf "Select an Audio Package:\n1) alsa\n2) jack\n3) pipewire\n4) pulseaudio\n-> " && read -r AUDIO
		case "$AUDIO" in
			1)
				artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S alsa-lib alsa-oss alsa-utils alsa-plugins"
				;;
			2)
				artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S jack"
				;;
			3)
				artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S pipewire"
				;;
			4)
				artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S pulseaudio pavucontrol pulseaudio-alsa alsa-utils"
		esac
		clear

		# Installation of a Graphical Environment
		printf "Select a Graphical Environment:\n1) bspwm\n2) Cinnamon\n3) dwm (Requires Online Git Build)\n4) Gnome\n5) i3 [Gaps Edition]\n6) Kde\n7) LXDE\n8) LXQt\n9) Mate\n10) Openbox\n11) XFCE\n12) None\n-> " && read -r ENVIRONMENT

		case "$ENVIRONMENT" in
			1)
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S bspwm sxhkd"
				;;
			2)
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S cinnamon nemo-fileroller gnome-terminal"
				;;
			3)
				printf "Enter your dwm build location (e.g. https://git.suckless.org/dwm)\n-> " && read -r DWM
				artix-chroot /mnt /bin/bash -c "git clone $DWM /tmp/dwm && cd /tmp/dwm && make clean install && cd /"
				;;
			4)
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S gnome gnome-extra nautilus gnome-terminal"
				;;
			5)
				artix-chroot /mnt /bin/bash -c "pacman --needed -S i3 dmenu i3blocks"
				;;
			6)
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S plasma kdebase"
				;;
			7)
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S lxde"
				;;
			8)
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S lxqt"
				;;
			9)
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S mate mate-extra"
				;;
			10)
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S openbox obconf-qt menumaker"
				;;
			11)
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S xfce4 xfce4-goodies"
		esac
		clear

		# Installation of a Display Manager
		printf "Select your desired Display Manager\n1) Gdm\n2) Lightdm\n3) Sddm\n4) None\n-> " && read -r DISPLAY && clear
		case "$DISPLAY" in
			1)
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S gdm"
				clear
				case "$ARTIXINIT" in
					1)
						artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S gdm-dinit && ln -s /etc/dinit.d/gdm /etc/dinit.d/boot.d/"
						;;
					2)
						artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S gdm-openrc && rc-update add gdm default"
						;;
					3)
						artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S gdm-runit && ln -s /etc/runit/sv/gdm /etc/runit/runsvdir/default"
						;;
					4)
						artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S gdm-s6 && s6-rc-bundle-update -c /etc/s6/rc/compiled add default gdm"
						;;
					5)
						artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S gdm-suite66 && 66-enable -t default gdm"
				esac
				;;
			2)
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S lightdm lightdm-gtk-greeter"
				clear
				case "$ARTIXINIT" in
					1)
						artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S lightdm-dinit && ln -s /etc/dinit.d/lightdm /etc/dinit.d/boot.d/"
						;;
					2)
						artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S lightdm-openrc && rc-update add lightdm default"
						;;
					3)
						artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S lightdm-runit && ln -s /etc/runit/sv/lightdm /etc/runit/runsvdir/default"
						;;
					4)
						artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S lightdm-s6 && s6-rc-bundle-update -c /etc/s6/rc/compiled add default lightdm"
						;;
					5)
						artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S lightdm-suite66 && 66-enable -t default lightdm"
				esac
				;;
			3)
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S sddm"
				clear
				case "$ARTIXINIT" in
					1)
						artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S sddm-dinit && ln -s /etc/dinit.d/sddm /etc/dinit.d/boot.d/"
						;;
					2)
						artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S sddm-openrc && rc-update add sddm default"
						;;
					3)
						artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S sddm-runit && ln -s /etc/runit/sv/sddm /etc/runit/runsvdir/default"
						;;
					4)
						artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S sddm-s6 && s6-rc-bundle-update -c /etc/s6/rc/compiled add default sddm"
						;;
					5)
						artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S sddm-suite66 && 66-enable -t default sddm"
				esac
		esac
		clear
	fi

	# Installation of GRUB Bootloader
	printf "Choose the type of boot you want to use for grub\n1) BIOS\n2) EFI\n3) Auto\n-> " && read -r GRUB
	clear

	## Adding Encrypted Config to GRUB
	if [ "$ENCRYPTED" != "N" ] && [ "$ENCRYPTED" != "n" ]; then
		sed -i s:'GRUB_CMDLINE_LINUX=""':"GRUB_CMDLINE_LINUX=\"cryptdevice=/dev/$ENCRYPTEDPARTITION\:cryptdevice\"":g /mnt/etc/default/grub
	fi

	case "$GRUB" in
		1)
			artix-chroot /mnt /bin/bash -c "grub-install --target=i386-pc /dev/$SDX"
			artix-chroot /mnt /bin/bash -c "grub-mkconfig -o /boot/grub/grub.cfg"
			;;
		2)
			artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S efibootmgr"
			artix-chroot /mnt /bin/bash -c "grub-install --target=x86_46-efi /dev/$SDX"
			artix-chroot /mnt /bin/bash -c "grub-mkconfig -o /boot/grub/grub.cfg"
			;;
		*)
			artix-chroot /mnt /bin/bash -c "grub-install /dev/$SDX"
			artix-chroot /mnt /bin/bash -c "grub-mkconfig -o /boot/grub/grub.cfg"
	esac
	clear
}

extraPackages()
{
	echo "##################################"
	echo "# Installation of Extra Packages #"
	echo "##################################"
	echo
	echo
	sleep 2

	# Printer and Scanners
	printf "Do you want to install Printers & Scanners support? [Y/n]" && read -r CUPS
	if [ "$CUPS" != "N" ] && [ "$CUPS" != "n" ]; then
		artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S cups cups-pdf hplip system-config-printer simple-scan"
		clear
		case "$ARTIXINIT" in
			1)
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S cups-dinit avahi-dinit && ln -s /etc/dinit.d/cupsd /etc/dinit.d/boot.d/"
				;;
			2)
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S cups-openrc avahi-openrc && rc-update add cupsd default && rc-update add avahi default"
				;;
			3)
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S cups-runit avahi-runit && ln -s /etc/runit/sv/cupsd /etc/runit/runsvdir/default && ln -s /etc/runit/sv/runit /etc/runit/runsvdir/default"
				;;
			4)
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S cups-s6 avahi-s6 && s6-rc-bundle add default cupsd && s6-rc-bundle-update -c /etc/s6/rc/compiled add default avahi"
				;;
			5)
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S cups-suite66 avahi-suite66 && 66-enable -t default cupsd && 66-enable -t default avahi"
		esac
	fi
	clear

	printf "Do you want to install Arch Linux Repos? [Y/n]" && read -r ARCH
	if [ "$ARCH" != "N" ] && [ "$ARCH" != "n" ]; then
		echo
		echo "############################################"
		echo "# Installation of Arch Linux Support Repos #"
		echo "############################################"
		echo
		echo
		sleep 2

		# Adding Arch Linux Support
		artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -Syu artix-archlinux-support"
		printf "\n#[testing]\n#Include = /etc/pacman.d/mirrorlist-arch\n\n[extra]\nInclude = /etc/pacman.d/mirrorlist-arch\n\n[community]\nInclude = /etc/pacman.d/mirrorlist-arch\n\n#[community-testing]\n#Include = /etc/pacman.d/mirrorlist-arch" >> /mnt/etc/pacman.conf
		artix-chroot /mnt /bin/bash -c "pacman-key --populate archlinux"
		mv /mnt/etc/pacman.d/mirrorlist-arch /mnt/etc/pacman.d/mirrorlist-arch.backup
		clear

		echo "Updating Arch Linux Repos..."
		grep -vi ^# /mnt/etc/pacman.d/mirrorlist-arch.backup | rankmirrors - > /mnt/etc/pacman.d/mirrorlist-arch
		clear

		# Installation of 32 bit repo
		printf "Do you want to multilib repo? [Y/n]" && read -r MULTI
		if [ "$MULTI" != "N" ] && [ "$MULTI" != "n" ]; then
			printf "\n[multilib]\nInclude = /etc/pacman.d/mirrorlist-arch" >> /mnt/etc/pacman.conf
			artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -Syu"
		fi
		clear

		# Gaming Related Stuff
		if [ "$MULTI" != "N" ] && [ "$MULTI" != "n" ]; then
			printf "Do you want to install Gaming software and related dependencies? (steam, lutris, 32 bit libraries) [Y/n]" && read -r GAME
			if [ "$GAME" != "N" ] && [ "$GAME" != "n" ]; then
				artix-chroot /mnt /bin/bash -c "pacman --needed --noconfirm -S wine-staging wine-mono wine-gecko winetricks giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse libgpg-error lib32-libgpg-error alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo sqlite lib32-sqlite libxcomposite lib32-libxcomposite libxinerama lib32-libgcrypt libgcrypt lib32-libxinerama ncurses lib32-ncurses opencl-icd-loader lib32-opencl-icd-loader libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader lutris"
				clear

				printf "Select a graphics card brand:\n1)AMD\n2)Intel\n3)NVIDIA\n-> " && read -r GPU
				case "$GPU" in
					1)
						artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -S lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader"
						;;
					2)
						artix-chroot /mnt /bin/bash -c "pacman --noconfirm -S lib32-mesa vulkan-intel lib32-vulkan-intel vulkan-icd-loader lib32-vulkan-icd-loader"
						;;
					3)
						artix-chroot /mnt /bin/bash -c "pacman --noconfirm -S nvidia nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader"
				esac
			fi
		fi
	fi
	clear

	# AUR
	printf "Do you want to Enable AUR support? [Y/n] (paru)" && read -r AUR
	if [ "$AUR" != "N" ] && [ "$AUR" != "n" ]; then
		artix-chroot /mnt /bin/bash -c "pacman --noconfirm --needed -Sy curl"
		clear
		artix-chroot /mnt /bin/bash -c "sudo -u $USERNAME mkdir /home/$USERNAME/AUR && cd /home/$USERNAME/AUR && sudo -u $USERNAME curl -LO 'https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=paru' && sudo -u $USERNAME mv PKGBUILD\?h\=paru PKGBUILD && sudo -u $USERNAME makepkg --noconfirm -si && cd / && rm -rf /home/$USERNAME/AUR /home/$USERNAME/.cargo"
	fi
	clear

	# Extra Programs
	printf "Do you want to install any extra packages not mentioned above? [Y/n]" && read -r MISSING
	if [ "$MISSING" != "N" ] && [ "$MISSING" != "n" ]; then
		printf "Type below the extra packages you want to install separated by spaces:\n-> " && read -r PACKAGES
		artix-chroot /mnt /bin/bash -c "pacman --needed -S $PACKAGES"
	fi
	clear
}

systemTweaks()
{
	echo "###############################"
	echo "# Updating System Services... #"
	echo "###############################"
	echo
	echo
	sleep 2

	printf "Do you want to Disable internal system beep? [Y/n]" && read -r BEEP
	if [ "$BEEP" != "N" ] && [ "$BEEP" != "n" ]; then
		artix-chroot /mnt /bin/bash -c "echo "blacklist pcspkr" > /mnt/etc/modprobe.d/nobeep.conf"
	fi
	clear
}


cleanup()
{
	echo "##################"
	echo "# Cleaning Up... #"
	echo "##################"
	echo
	echo
	sleep 2

	# Upgrading Packages and Deleting Leftovers
	artix-chroot /mnt /bin/bash -c "pacman --noconfirm -Syyuu"
	clear

	artix-chroot /mnt /bin/bash -c "pacman --noconfirm -Scc"
	clear
}

unmount()
{
	umount -R /mnt

	if [ "$WIPENCRYPT" != "N" ] && [ "$WIPENCRYPT" != "n" ]; then
		cryptsetup close cryptdevice
	fi

	swapoff "$SWAP"
	clear
	echo "###########"
	echo "# DONE :) #"
	echo "###########"
	echo "           "
	echo "Installation Complete"
	echo "System Will Shutdown in a minute"
	poweroff
}

### Main Script ###
artixLogo
connection
vars
partitioning
base
packages
extraPackages
systemTweaks
cleanup
unmount
