# Distro Bootstrap Scripts
This script automatically install and sets up one of the supported linux distributions with a working environment completely customized by the user throughout the install process while only using the vanilla programs.

![Logo](images/logo.png)

## How to Use
On a fresh install of your desired **GNU/Linux Supported Distribution** connect to the internet and then type:

```
curl -LO https://cyka-blyad.gitlab.io/install/*distro codename*
./*distro codename*
```

## Supported Distros for now:

* Arch GNU/Linux;
* Artix GNU/Linux;
* Parabola GNU/Linux.

### Contributors:

* [\@rafa_99](https://gitlab.com/rafa_99);
* [\@dutchmans](https://gitlab.com/dutchmans).
